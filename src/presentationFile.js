import { addLetter, userGuessedWord, backspace } from "./businessFile.js";

async function identifyPressedKey() {
  document.addEventListener("keydown", function handleKeyPressed(event) {

    const action = event.key;

    if (action === "Enter") {
      userGuessedWord();
    } else if (action === "Backspace") {
      backspace();
    } else if (isLetter(action)) {
      addLetter(action.toUpperCase());
    } else {
      // alert("Please press alphabet");
    }
  });
}

function isLetter(letter) {
  return /^[a-zA-Z]$/.test(letter);
}

identifyPressedKey();
