async function grabWordOfTheDay() {
  const res = await fetch("https://words.dev-apis.com/word-of-the-day").catch(
    function (error) {
      alert("404 error Url: is not working!");
    }
  );
  const resObj = await res.json();
  word = resObj.word.toUpperCase();
  return word;
}

async function validationWord(userGuessed) {
  
  const res = await fetch("https://words.dev-apis.com/validate-word", {
    method: "POST",
    body: JSON.stringify({ word: userGuessed }),
  }).catch(function (error) {
    alert("404 error Url: is not working!");
  });

  const resObj = await res.json();

  const validword = resObj.validWord;

  return validword;

}
export { grabWordOfTheDay, validationWord};
