import { grabWordOfTheDay, validationWord } from "./apiCall.js";

const displayLetter = document.querySelectorAll(".letter_box");
console.log(displayLetter);
const loader_div = document.querySelector(".loader");

let userGuessed = "";
let currentRow = 0;
let finalWord = "";

const ANSWER_LENGTH = 5;
const ROUNDS = 6;
let isLoading = true;
let done = false;
let word = "";
async function addLetter(letter) {
  if (userGuessed.length < ANSWER_LENGTH) {
    userGuessed += letter;
  } else {
    userGuessed = userGuessed.substring(0, userGuessed.length - 1) + letter;
  }
  displayLetter[ANSWER_LENGTH * currentRow + userGuessed.length - 1].innerText =
    letter;
}

function markInvalidWord() {
  for (let index = 0; index < ANSWER_LENGTH; index++) {
    displayLetter[currentRow * ANSWER_LENGTH + index].classList.remove(
      "invalid_word_flash"
    );
    setTimeout(
      () =>
        displayLetter[currentRow * ANSWER_LENGTH + index].classList.add(
          "invalid_word_flash"
        ),
      20
    );
  }
}

function setLoading(isLoading) {
  loader_div.classList.toggle("hidden", !isLoading);
}

async function userGuessedWord() {
  if (userGuessed.length !== ANSWER_LENGTH) {
    alert("Please enter 5 letter word!");
    return;
  }

  let callValidateWord = await validationWord(userGuessed);

  if (!callValidateWord) {
    markInvalidWord();
    alert("❌ Invalid word!");
    newGame();
    return;
  }
  word = await grabWordOfTheDay();
  finalWord = word.split("");

  const guessedLettersOfTheWord = userGuessed.split("");
  let allCorrect = 0;
  for (let index = 0; index < ANSWER_LENGTH; index++) {
    if (guessedLettersOfTheWord[index] === finalWord[index]) {
      allCorrect++;
      displayLetter[currentRow * ANSWER_LENGTH + index].classList.add(
        "correct"
      );
      if (allCorrect === ANSWER_LENGTH) {
        alert("Congratulations! You won🎉⭐⭐⭐");
        document.querySelector(".header_title").classList.add("winner");
        return;
      }
    } else if (finalWord.includes(guessedLettersOfTheWord[index])) {
      displayLetter[currentRow * ANSWER_LENGTH + index].classList.add(
        "missedplace"
      );
    } else {
      displayLetter[currentRow * ANSWER_LENGTH + index].classList.add("wrong");
    }
  }
  currentRow++;
  userGuessed = "";
  if (currentRow === ROUNDS) {
    alert(`You lose!🙁 The word was ${word}`);
    newGame();
    return;
  }
}

function newGame() {
  for (let index = 0; index < userGuessed.length; index++) {
    displayLetter[
      ANSWER_LENGTH * currentRow + userGuessed.length - index - 1
    ].innerHTML = "";
  }
  userGuessed = "";
}

export function makeMapOfWord(array) {
  console.log(array);
  const obj = {};
  for (let index = 0; index < array.length; index++) {
    const letter = array[index];
    if (obj[letter]) {
      console.log(obj[letter]);
      obj[letter]++;
    } else {
      obj[letter] = 1;
    }
  }
  console.log(obj);
  return obj;
}

async function backspace() {
  userGuessed = userGuessed.substring(0, userGuessed.length - 1);
  displayLetter[ANSWER_LENGTH * currentRow + userGuessed.length].innerText = "";
}

export { backspace, userGuessedWord, markInvalidWord, addLetter };
