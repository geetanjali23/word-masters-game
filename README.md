# Word Masters Game (Clone of the Wordle Game)

### Project Description

- This is Word guess game. 
- User have to guess the word of five letters. The answer word is called Word of the day.
- Total guess chances is 6. After that you'll lose the game.
- If user will put the invalid word the red flash will visible for some seconds.
- If the guessed word contains the letter which is present in the answer word that word block will visible yellow.
- If the guessed word doesn't contain any word of the word of the day, the blocks will look grey in color.
- If the guessed word is correct, the box will look green and the Title "Word Masters" color will turn into rainbow color.

